const logger = {};

logger.info = function (message) {
  log(' INFO: ', message);
};

logger.error = function (message) {
  log(' ERROR: ', message);
}

logger.warn = function (message) {
  log(' WARN: ', message);
}

function log(level, message) {
  const date = '[' + (new Date()).toISOString() + ']';
  console.log(date + level + message);
}

module.exports = logger;