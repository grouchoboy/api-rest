var express = require('express');

var models = require('../models');
var router = express.Router();


router.get('/', function (req, res) {
  var p = models.Film.findAll({
    limit: 6
  });

  p.then(function (films) {
    var response = {
      data: films,
      next: 'localhost:3000/films/page/1'
    };
    res.status(200);
    res.send(response);
  });
  p.catch(function (err) {
    res.status(500);
    res.end();
    return;
  });
});

function getNameAndPassword(auth) {
  const base64 = auth.substr('Basic'.length);
  const ascii = new Buffer(base64, 'base64').toString('ascii');
  const username = ascii.split(':')[0];
  const password = ascii.split(':')[1];
  return { username: username, password: password };
}

function isAdmin(auth) {
  const data = getNameAndPassword(req.get('Authorization'));
  if (data.username === 'admin' || data.password === '123456') {
    return true;
  }
  return false;
}

router.get('/:id/reviews', function(req, res) {
  const id = req.params.id;
  var p = models.Film.getReviews(id);
  p.then(function(reviews) {
    res.status(200);
    res.send(reviews);
  }).catch(function(error) {
    console.log(error);
  });
});

router.get('/:id', function(req, res) {
  const id = req.params.id;
  const p = models.Film.findOne({ where: { id: id } });
  p.then(function(film) {
    if (film !== null) {
      res.status(200);
      res.send(film);
    } else {
      res.status(404);
      res.end();
    }
  });
});

router.delete('/:id', function (req, res) {
  if (isAdmin(req.get('Authorization'))) {
    // Habría que buscar y si existe borrar la película.
    res.status(204);
    res.end();
  }
  res.status(401);
  res.end();
});

router.post('/', function (req, res) {
  if (isAdmin(req.get('Authorization'))) {
    // Habría que crear la película con los parámetros del cuerpo
    // de la petición.
    res.status(201);
    res.end();
  }
  res.status(401);
  res.end();
})

router.get('/page/:num', function (req, res) {
  const page = parseInt(req.params.num);
  var p = models.Film.findAll({
    offset: page * 3,
    limit: 3
  });
  const nextPage = page + 1;
  const previousPage = page - 1;
  p.then(function (films) {
    var response = {
      data: films,
      next: 'localhost:3000/films/page/' + nextPage,
      previous: 'localhost:3000/films/page/' + previousPage
    }

    if (page === 0) {
      response.previous = undefined;
    }
    res.status(200);
    res.send(response);
  });
  p.catch(function (err) {
    console.log(err);
    res.status(500);
    res.end();
  });
});


module.exports = router;
