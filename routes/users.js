var express = require('express');
var bodyParser = require('body-parser');

var models = require('../models');

var router = express.Router();
var User = models.User;
var Review = models.Review;


function getNameAndPassword(auth) {
  const base64 = auth.substr('Basic'.length);
  const ascii = new Buffer(base64, 'base64').toString('ascii');
  const username = ascii.split(':')[0];
  const password = ascii.split(':')[1];
  return { username: username, password: password };
}

router.get('/login', function(req, res) {
  console.log(req.get('Authorization'));
  const data = getNameAndPassword(req.get('Authorization'));
  console.log(data);
  if (data.password !== '123456') {
    res.status(403);
    res.send();
    return;
  }

  res.status(200);
  res.send();
});

router.delete('/', function (req, res) {
  const data = getNameAndPassword(req.get('Authorization'));
  if (data.password !== '123456') {
    res.status(401);
    res.end();
    return;
  }
  var p = User.destroy({ where: { username: data.username } });
  p.then(function (affectedRows) {
    if (affectedRows === 1)
      res.status(204);
    else if (affectedRows === 0)
      res.status(404);
      return;
  });
  res.status(204);
  res.end();
  p.catch(function (err) {
    res.status(500);
    res.end();
  });
});

router.put('/:username', function (req, res) {
  const data = getNameAndPassword(req.get('Authorization'));
  console.log('Username: ' + data.username + '\nPassword: ' + data.password);

  if (data.username !== req.params.username || data.password !== '123456') {
    res.status(401);
    res.end();
    return;
  }

  var p = User.findOne({ where: { username: data.username } });
  p.then(function (user) {
    if (user === null) {
      res.status(401);
      res.end();
      return;
    }

    var update = user.update({
      username: req.body.username,
      description: req.body.description });

    update.then(function () {
      res.status(204);
      res.end();
    });
    update.catch(function (err) {
      res.status(500);
      res.end();
    });
  });
});


router.post('/', function (req, res) {
  var username = req.body.username;
  console.log(username);
  var p = models.User.create({ username: username});
  p.then(function () {
    res.status(201);
    res.end();
  });

  p.catch(function (error) {
    res.status(500);
    res.end();
  });
});

/* GET users listing. */
router.get('/', function(req, res) {
  var p = models.User.findAll({
    include: models.Review,
    limit: 3
   });
  p.then(function (users) {
    users.forEach(manipulate);
    var usersResponse = users.map(manipulate);
    var response = {
      data: usersResponse,
      next: 'localhost:3000/users/page/1'
    }
    res.status(200);
    res.send(response);
  });
  p.catch(function (error) {
    done(error);
  });
});

router.get('/:username', function (req, res) {
  var p = User.findOne({ where: { username: req.params.username } });
  p.then(function (user) {
    if (user !== null) {
      res.status(200);
      res.send(user);
    } else {
      res.status(404);
      res.end();
    }
  });
});

router.get('/page/:num', function (req, res) {
  const page = parseInt(req.params.num);
  var p = User.findAll({
    include: Review,
    offset: page * 3,
    limit: 3
  });

  p.then(function (users) {
    users.forEach(manipulate);
    var usersResponse = users.map(manipulate);
    const nextPage = page + 1;
    const previousPage = page - 1;
    var response = {
      data: usersResponse,
      next: 'localhost:3000/users/page/' + nextPage,
      previous: 'localhost:3000/users/page/' + previousPage,
    }

    if (page === 0) {
      response.previous = undefined;
    }
    res.status(200);
    res.send(response);
  });

  p.catch(function (error) {
    console.log(error);
    res.status(500);
    res.end();
  });
});

router.get('/:username/reviews', function (req, res) {
  const p = models.User.getReviews(req.params.username);
  p.then(function (reviews) {
    console.log(reviews);
    res.status(200);
    res.send(reviews);
  });
  p.catch(function (errror) {
    console.log(error);
  });
});

router.get('/:username/reviews/:id', function (req, res) {
  var p = models.Review.findAll({
    where: {
      id: parseInt(req.params.id)
    }
  });
  p.then(function (reviews) {
    res.status(200);
    res.send(reviews);
  });
  p.catch(function (error) {
    console.log(error);
  });
});

function manipulate(user, index, array) {
  user = user.toJSON();
  user.createdAt = undefined;
  user.updatedAt = undefined;


  var reviewsResponse = user.Reviews.map(function (review) {
    review.createdAt = undefined;
    review.updatedAt = undefined;
    review.body = undefined;
    review.FilmId = undefined;
    review.UserId = undefined;
    review.href = "localhost:3000/users/" + user.username + "/reviews/" + review.id;

    return review;
  });
  return user;
}

module.exports = router;
