var models = require('../models');
var express = require('express');
const logger = require('../util/logger');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  models.User.findAll().then(function (users) {
    logger.info("Getting all users");
    res.send(users);
  });
});

module.exports = router;
