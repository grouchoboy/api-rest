var models = require('../models');
var express = require('express');
var router = express.Router();


function getNameAndPassword(auth) {
  const base64 = auth.substr('Basic'.length);
  const ascii = new Buffer(base64, 'base64').toString('ascii');
  const username = ascii.split(':')[0];
  const password = ascii.split(':')[1];
  return { username: username, password: password };
}

function getUserId(userData, callback) {
  if (userData.password !== '123456' ) {
    return null;
  }

  var p = models.User.findOne({ where: { username: userData.username } });
  p.then(function (user) {
    console.log(user);
    if (user === null) {
      return null;
    }

    console.log("Id del usuaario: " + user.id);
    callback(user.id);
  });
  p.catch(function (err) {
    console.log(err);
    return null;
  });
}

router.post('/', function (req, res) {
  const userData = getNameAndPassword(req.get('Authorization'));
  console.log(userData);

  const filmId = parseInt(req.body.filmId);
  console.log("id de ususario: " + userId);
  const title = req.body.title;
  const body = req.body.body;

  const userId = getUserId(userData, function (id) {
    if (id === null) {
      res.status(401);
      res.end();
      return;
    }
    var p = models.Review.create({
      title: title,
      body: body,
      FilmId: filmId,
      UserId: id
    });
    p.then(function () {
      res.status(201);
      res.end();
    });
    p.catch(function (err) {
      console.log(err);
      res.status(500);
      res.end();
    });
  });
});

module.exports = router;
