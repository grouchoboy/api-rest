var app = require('../app');
var supertest = require('supertest');

describe('prueba de peliculas', function () {
  it('/films devuelve el contenido adecuado', function (done) {
    supertest(app)
      .get('/films')
      .expect(200, done);
  });

  it('borrar una pelicula', function (done) {
    supertest(app)
      .delete('/films/1', {
        'auth': {
          'user': 'admin',
          'pass': '123456'
        }
      })
      .expect(204, done);
  });

  it('crear una película', function (done) {
    supertest(app)
      .post('/films')
      .expect(201, done);
  });
});
