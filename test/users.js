var app = require('../app');
var supertest = require('supertest');

describe('prueba de usuarios', function () {
  it('/users devuelve el contenido adecuado', function (done) {
    supertest(app)
      .get('/users')
      .expect(200, done);
  });

  it('Obtener un usuario que existe por username', function (done) {
    supertest(app)
      .get('/users/manu')
      .expect(200, done);
  });

  it('Pedir un usuario que no existe', function (done) {
    supertest(app)
      .get('/users/345345')
      .expect(404, done);
  });

  it('Crear un usuario', function (done) {
    supertest(app)
      .post('/users')
      .field('username', 'usuariotest')
      .expect(201, done);
  });

  it('Obtener una review concreta de un usuario concreto', function (done) {
    supertest(app)
      .get('/users/manu/reviews/1')
      .expect(200, done);
  })

  it('Actualizar el perfil de un usuario', function (done) {
    supertest(app)
      .put('manu@123456/users')
      .expect(204, done);
  });
});
