'use strict';
module.exports = function(sequelize, DataTypes) {
  var Film = sequelize.define('Film', {
    title: DataTypes.STRING,
    description: DataTypes.TEXT
  }, {
    classMethods: {
      associate: function(models) {
        Film.hasMany(models.Review);
      },

      getReviews: function(filmId) {
        return sequelize.query(
          `SELECT u.username, r.title, r.body, f.id
           FROM Reviews as r
           JOIN Users as u ON (r.UserId = u.id)
           JOIN Films as f ON (r.FilmId = f.id)
           WHERE f.id = ?;`, { replacements: [filmId], type: sequelize.QueryTypes.SELECT });
      }
    }
  });

  return Film;
};
