'use strict';

module.exports = function(sequelize, DataTypes) {
  var User = sequelize.define('User', {
    username: DataTypes.STRING,
    description: DataTypes.TEXT
  }, {
    classMethods: {
      associate: function (models) {
        User.hasMany(models.Review);
      },

      getReviews: function (username) {
        return sequelize.query(
          `SELECT f.title AS "film", r.title, r.body, r.id
           FROM Reviews as r
           JOIN Films as f ON (r.FilmId = f.id)
           JOIN Users as u ON (r.UserId = u.id)
           WHERE u.username LIKE ?;`, { replacements: [username],
            type: sequelize.QueryTypes.SELECT });
      }
    }
  });
  return User;
};
