'use strict';

var models = require('../models');

module.exports = {
  up: function (queryInterface, Sequelize) {
    var p = models.Film.bulkCreate([
      { title: 'Batman', description: `Sed ut perspiciatis unde omnis iste natus error sit v
      oluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo
      inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim i
      psam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur
      magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui
      dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius
      modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim
      ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi
      ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse
      quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?`
    },
      { title: 'Dracula', description: `Sed ut perspiciatis unde omnis iste natus error sit v
      oluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo
      inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim i
      psam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur
      magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui
      dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius
      modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim
      ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi
      ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse
      quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?`
    },
      { title: 'La vida de Brian', description: `Sed ut perspiciatis unde omnis iste natus error sit v
      oluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo
      inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim i
      psam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur
      magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui
      dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius
      modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim
      ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi
      ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse
      quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?`
    }]
    );

    return p.then(function () {
      models.Review.bulkCreate([
        {title: 'buena', body: `Sed ut perspiciatis unde omnis iste natus error sit v
      oluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo
      inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim i
      psam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur
      magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui
      dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius
      modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim
      ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi
      ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse
      quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?`,
      FilmId: 1,
      UserId: 1},

        {title: 'mala', body: `Sed ut perspiciatis unde omnis iste natus error sit v
      oluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo
      inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim i
      psam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur
      magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui
      dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius
      modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim
      ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi
      ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse
      quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?`, FilmId: 2, UserId: 1},

        {title: 'regular', body: `Sed ut perspiciatis unde omnis iste natus error sit v
      oluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo
      inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim i
      psam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur
      magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui
      dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius
      modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim
      ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi
      ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse
      quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?`, FilmId: 3, UserId: 2}]
      );
    });
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('Films', null, {})
      .then(function () {
        queryInterface.bulkDelete('Reviews', null, {});
      });
  }
};
