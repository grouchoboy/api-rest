'use strict';

var models = require('../models');

module.exports = {
  up: function (queryInterface, Sequelize) {
    return models.Film.bulkCreate([
      { title: 'Hola Cabesa', description: `Sed ut perspiciatis unde omnis iste natus error sit v
      oluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo
      inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim i
      psam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur
      magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui
      dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius
      modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim
      ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi
      ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse
      quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?`},

      { title: 'El retorno del jedi', description: `Sed ut perspiciatis unde omnis iste natus error sit v
      oluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo
      inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim i
      psam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur
      magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui
      dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius
      modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim
      ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi
      ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse
      quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?`},

      { title: 'El rey león', description: `Sed ut perspiciatis unde omnis iste natus error sit v
      oluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo
      inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim i
      psam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur
      magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui
      dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius
      modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim
      ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi
      ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse
      quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?`},

      { title: 'Ocho apellidos vascos', description: `Sed ut perspiciatis unde omnis iste natus error sit v
      oluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo
      inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim i
      psam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur
      magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui
      dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius
      modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim
      ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi
      ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse
      quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?`},

      { title: 'Star Trek: into the darkness', description: `Sed ut perspiciatis unde omnis iste natus error sit v
      oluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo
      inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim i
      psam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur
      magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui
      dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius
      modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim
      ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi
      ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse
      quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?`},

      { title: 'Sala 24h umh', description: `Sed ut perspiciatis unde omnis iste natus error sit v
      oluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo
      inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim i
      psam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur
      magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui
      dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius
      modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim
      ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi
      ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse
      quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?`},

      { title: 'Prácticas', description: `Sed ut perspiciatis unde omnis iste natus error sit v
      oluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo
      inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim i
      psam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur
      magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui
      dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius
      modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim
      ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi
      ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse
      quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?`},

      { title: 'Rework', description: `Sed ut perspiciatis unde omnis iste natus error sit v
      oluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo
      inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim i
      psam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur
      magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui
      dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius
      modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim
      ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi
      ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse
      quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?`},

      { title: 'Mundial motogp', description: `Sed ut perspiciatis unde omnis iste natus error sit v
      oluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo
      inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim i
      psam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur
      magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui
      dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius
      modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim
      ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi
      ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse
      quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?`}
    ])
  },

  down: function (queryInterface, Sequelize) {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
  }
};
