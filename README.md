# Diseño de APIs REST

La práctica se encuentra en un repositorio git en:
 https://gitlab.com/grouchoboy/api-rest

## Casos de uso


1. Obtener el perfil de todos los usuarios. El perfil consta de el `username`
 y de las reviews que ha escrito dicho usuario, estas reviews mostrarán
 su título y la dirección al recurso para acceder a cada una de ellas.
 estos usuarios se obtendrán de forma paginada de tres en tres. (get)

2. Obtener una review concreta de un usuario concreto. (get)

3. Crear un usuario. (post)

4. Actualizar el perfil de usuario, para poder realizar esta acción hay que
  estar autenticado como el usuario que se quiere actualizar. (put)

5. Listar todas las películas. Se obtienen de forma paginada de tres en tres. (get)

6. Crear una nueva review. Para poder realizar la acción como parámetros dentro
del body de la petición se debe pasar un json con el titulo de la review, el
cuerpo de la review y el id de la película a la cual hace referencia la review.
Para crear la review hará falta estar autenticado. (post)

7. Se puede borrar un usuario, para ello el usuario debe autenticarse. No es
necesario indicar ningún parámetro, ya que se borra el usuario al que hace
referencia la autenticación. (delete)

8. Se puede obtener el perfil de un usuario mediante su `username`. (get)

9. Un administrador puede borrar una película. Para poder hacerlo debe
autenticarse como administrador. Para el API simplemente con indicar en la
cabecera `Authorization` que se llama admin con contraseña 123456 es suficiente
(delete)

10. Un administrador puede crear una película. Debe cumplir el mismo
requisito que en el caso de usu anterior.

## Requisitos adicionales

Casos de uso en los que se utiliza persistencia con la base de datos:
1, 2, 3, 4, 5, 6, 7, 8.

El API está divido usando routing modular.

El API está disponible en la siguiente dirección: http://manupascual.me:3000
En servidor dónde está alojado tengo alguna otra cosa funcionando y va un poco
justo de RAM. Igual es posible que se caiga el proceso, estaré atento de que si
pasa ponerlo en marcha enseguida.

## Puesta en marcha

Para poder poner en funcionamiento el api hay que realizar los
siguientes comandos:

    $ npm install
    $ sequelize db:migrate
    $ sequelize db:seed
